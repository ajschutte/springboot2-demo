package com.mercurygate.demo;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * @author Andries on 2/8/18.
 */
public class EmbeddedTomcatWithoutSpringTests {

    private static final Logger logger = LoggerFactory.getLogger(EmbeddedTomcatWithoutSpringTests.class);

    private static Tomcat tomcat;

    @BeforeClass
    public static void init() {

        logger.info("initializing..");

        tomcat = new Tomcat();
        tomcat.setPort(8080);
        File base = new File(System.getProperty("java.io.tmpdir"));
        Context rootCtx = tomcat.addContext("", base.getAbsolutePath());
        Tomcat.addServlet(rootCtx, "demoServlet", new DemoServlet());
        rootCtx.addServletMappingDecoded("/demo/*", "demoServlet");
        try {
            tomcat.start();
        }
        catch (LifecycleException e) {
            logger.error("Error in init()..", e);
        }
        tomcat.getServer().await();

    }

    @AfterClass
    public static void shutdown() {

        logger.info("shutting down..");

        try {
            tomcat.stop();
        }
        catch (LifecycleException e) {
            logger.error("Error in shutdown()..", e);
        }

    }




}
