package com.mercurygate.demo.rg;

import java.time.Duration;
import java.time.Instant;
import java.util.LinkedHashSet;
import java.util.Set;

import org.lognet.springboot.grpc.GRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.grpc.stub.StreamObserver;

@GRpcService
public class RouteGrpcController extends RouteServiceGrpc.RouteServiceImplBase {

    private Logger logger = LoggerFactory.getLogger(RouteGrpcController.class);

    private static Set<StreamObserver<RouteNote>> observers = new LinkedHashSet<>();

    @Override
    public StreamObserver<GeoPoint> recordRoute(StreamObserver<RouteSummary> responseObserver) {

        logger.info("Invoking recordRoute(..)..");

        return new StreamObserver<GeoPoint>() {

            int pointCount;
            int featureCount;
            int distance;

            GeoPoint previous;
            Instant start = Instant.now();

            @Override
            public void onNext(GeoPoint point) {

                logger.info("Invoking server callback onNext()..");

                pointCount++;
                if (containsFeature(point)) {
                    featureCount++;
                }
                // For each point after the first, add the incremental distance from the previous point
                // to the total distance value.
                if (previous != null) {
                    distance += calcDistance(previous, point);
                }
                previous = point;

            }

            @Override
            public void onError(Throwable t) {
                logger.error("Encountered error in recordRoute", t);
            }

            @Override
            public void onCompleted() {

                logger.info("Invoking server callback onCompleted()..");

                Instant end = Instant.now();
                long seconds = Duration
                        .between(start, end).getSeconds();
                responseObserver.onNext(RouteSummary.newBuilder().setPointCount(pointCount)
                                                    .setFeatureCount(featureCount).setDistanceInMeters(distance)
                                                    .setElapsedTimeInSecs((int) seconds).build());
                responseObserver.onCompleted();
            }
        };

    }

    @Override
    public StreamObserver<RouteNote> routeChat(StreamObserver<RouteNote> responseObserver) {

        logger.info("Invoking routeChat(..)..");

        observers.add(responseObserver);

        return new StreamObserver<RouteNote>() {
            @Override
            public void onNext(RouteNote note) {

                logger.info("Invoking server callback routeChat(..) onNext()..");

                // Respond with all previous notes at this location.
                for (StreamObserver<RouteNote> observer : observers) {
                    if (!observer.equals(responseObserver)) {
                        observer.onNext(RouteNote.newBuilder().setMessage(note.getMessage()).setPoint(note.getPoint()).build());
                    }
                }

            }

            @Override
            public void onError(Throwable t) {
                logger.error("Encountered error in routeChat(..)..", t);
                responseObserver.onError(t);
                observers.remove(responseObserver);
            }

            @Override
            public void onCompleted() {

                logger.info("Invoking server callback routeChat(..) onCompleted()..");

                responseObserver.onCompleted();
                observers.remove(responseObserver);
            }
        };

    }

    //dummy implementation
    private boolean containsFeature(GeoPoint point) {
        return true;
    }

    //dummy implementation
    private int calcDistance(GeoPoint previous, GeoPoint point) {
        return 1000;
    }


}
