package com.mercurygate.demo.rg;

import java.util.stream.IntStream;

import org.lognet.springboot.grpc.GRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.grpc.stub.StreamObserver;


@GRpcService
public class GeoFeatureGrpcController extends GeoFeatureServiceGrpc.GeoFeatureServiceImplBase {

    private Logger logger = LoggerFactory.getLogger(GeoFeatureGrpcController.class);

    private static final int STREAM_COUNT = 10;

    @Override
    public void createGeoFeature(GeoPoint request, StreamObserver<GeoFeature> responseObserver) {

        logger.info("Invoking createGeoFeature(..)..");

        responseObserver.onNext(checkFeatureFromPoint(request, "dummy"));
        responseObserver.onCompleted();

    }

    @Override
    public void createGeoFeaturesFromRectangle(GeoRectangle request, StreamObserver<GeoFeature> responseObserver) {

        logger.info("Invoking createGeoFeaturesFromRectangle(..)..");

        IntStream.range(0, STREAM_COUNT)
                 .mapToObj(i -> checkFeatureFromRectangle(request, "dummy" + i))
                 .forEach(responseObserver::onNext);

        responseObserver.onCompleted();

    }

    private GeoFeature checkFeatureFromPoint(GeoPoint point, String name) {

        // return a dummy feature.
        return GeoFeature
                .newBuilder().setName(name).setPoint(point).build();

    }

    private GeoFeature checkFeatureFromRectangle(GeoRectangle rectangle, String name) {

        // return a dummy feature.
        return GeoFeature.newBuilder().setName(name).setPoint(rectangle.getLoLeftPoint()).build();

    }

}
