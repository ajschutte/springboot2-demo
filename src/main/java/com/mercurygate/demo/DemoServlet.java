package com.mercurygate.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebServlet("/demo-servlet/*")
public class DemoServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(DemoServlet.class);

    @Autowired
    private DemoSingletonService singleton;

    @Autowired
    private ObjectMapper mapper;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        logger.info("INVOKING doGet(..) on servlet..");

        DemoServiceResponses backend =singleton.demo(Optional.empty());

        resp.setContentType("application/json");

        String response =  getJsonRepresentationForResource(backend);

        resp.getWriter().append(response);

    }

    private String getJsonRepresentationForResource(Object resource) {

        try {
            return mapper
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(resource);
        }
        catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }

}
