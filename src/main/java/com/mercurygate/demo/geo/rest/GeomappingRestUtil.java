package com.mercurygate.demo.geo.rest;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Andries on 10/24/17.
 */
public class GeomappingRestUtil {

    public static List<com.mg.geomapping.models.Geomapping> assignRandomGeomappings(List<com.mg.geomapping.models.LocationAddress> locs) {

        List<com.mg.geomapping.models.Geomapping> result = new LinkedList<>();
        for (com.mg.geomapping.models.LocationAddress loc : locs) {
            com.mg.geomapping.models.Geomapping gm = new com.mg.geomapping.models.Geomapping()
                    .withLocationAddress(loc)
                    .withLatitude(ThreadLocalRandom.current().nextDouble(-90.0, +90.0))
                    .withLongitude(ThreadLocalRandom.current().nextDouble(-180.0, +180.0));
            result.add(gm);
        }

        return result;
    }

    public static List<com.mg.geomapping.models.LocationAddress> generateRandomLocationAddresses() {

        List<com.mg.geomapping.models.LocationAddress> result = new LinkedList<>();

        for (int i = 0; i < 200; i++) {
            com.mg.geomapping.models.LocationAddress la = new com.mg.geomapping.models.LocationAddress()
                    .withStreet(randomString())
                    .withCity(randomString())
                    .withStateOrProvince(randomString())
                    .withZipOrPostalCode("" + ThreadLocalRandom.current().nextInt(10000, 99999))
                    .withCountryCode(randomString());

            result.add(la);

        }

        return result;
    }

    private static String randomString() {
        return UUID.randomUUID().toString();
    }

}
