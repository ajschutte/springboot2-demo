package com.mercurygate.demo.geo.rest;

import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mg.geomapping.models.Geomapping;
import com.mg.geomapping.models.LocationAddress;

@RestController
public class GeomappingRestController {

    private static final Logger logger = LoggerFactory.getLogger(GeomappingRestController.class);

    private List<Geomapping> listResponse = null;

    @PostConstruct
    public void init() throws Exception {

        logger.info("Invoking init()..");

        List<LocationAddress> la = GeomappingRestUtil.generateRandomLocationAddresses();

        listResponse = GeomappingRestUtil.assignRandomGeomappings(la);

    }

    @RequestMapping(value = "/geomapping/fromLocationAddresses",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.POST)
    public ResponseEntity<List<Geomapping>> createGeomappings(
            @RequestBody List<LocationAddress> locationAddresses) {

        logger.info("Invoking createGeomappings(..)..");

        return new ResponseEntity<>(listResponse, HttpStatus.OK);

    }

}
