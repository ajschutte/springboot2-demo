package com.mercurygate.demo.geo.client;

import static com.mg.api.supported.headers.SupportedHeaders.DEFAULT_ACCEPT_HEADER;
import static com.mg.api.supported.headers.SupportedHeaders.DEFAULT_CONTENT_TYPE_HEADER;

import java.util.List;

import feign.Headers;
import feign.RequestLine;

public interface GeomappingClient {

    @RequestLine("POST /geomapping/fromLocationAddresses")
    @Headers({
            DEFAULT_ACCEPT_HEADER, DEFAULT_CONTENT_TYPE_HEADER
    })
    List<com.mg.geomapping.models.Geomapping> createGeomappings(List<com.mg.geomapping.models.LocationAddress> locationAddresses);

}
