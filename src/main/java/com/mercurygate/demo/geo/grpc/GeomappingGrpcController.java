package com.mercurygate.demo.geo.grpc;

import java.util.List;

import javax.annotation.PostConstruct;

import org.lognet.springboot.grpc.GRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mercurygate.demo.geo.GeomappingList;
import com.mercurygate.demo.geo.GeomappingServiceGrpc;
import com.mercurygate.demo.geo.LocationAddress;
import com.mercurygate.demo.geo.LocationAddressList;

import io.grpc.stub.StreamObserver;

/**
 * @author Andries on 10/24/17.
 */
@GRpcService
public class GeomappingGrpcController extends GeomappingServiceGrpc.GeomappingServiceImplBase {

    private Logger logger = LoggerFactory.getLogger(GeomappingGrpcController.class);

    private GeomappingList listResponse = null;

    @PostConstruct
    public void init() {

        logger.info("Invoking init()..");

        List<LocationAddress> la = GeomappingGrpcUtil.generateRandomLocationAddresses();

        GeomappingList.Builder responseBuilder = GeomappingList.newBuilder();

        GeomappingGrpcUtil.assignRandomGeomappings(la).forEach(responseBuilder::addGeomapping);

        listResponse = responseBuilder.build();

    }


    @Override
    public void createGeomappings(LocationAddressList request, StreamObserver<GeomappingList> responseObserver) {

        logger.info("Invoking createGeomappings(..)..");

        responseObserver.onNext(listResponse);

        responseObserver.onCompleted();

    }
}
