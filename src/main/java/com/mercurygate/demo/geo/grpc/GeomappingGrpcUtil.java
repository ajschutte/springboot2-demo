package com.mercurygate.demo.geo.grpc;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import com.mercurygate.demo.geo.Geomapping;
import com.mercurygate.demo.geo.LocationAddress;

public class GeomappingGrpcUtil {

    public static List<Geomapping> assignRandomGeomappings(List<LocationAddress> locs) {

        List<Geomapping> result = new LinkedList<>();
        for (LocationAddress loc : locs) {
            Geomapping gm = Geomapping.newBuilder()
                                      .setLocationAddress(loc)
                                      .setLatitude(ThreadLocalRandom
                                                           .current().nextDouble(-90.0, +90.0))
                                      .setLongitude(ThreadLocalRandom.current().nextDouble(-180.0, +180.0))
                                      .build();

            result.add(gm);
        }

        return result;
    }

    public static List<LocationAddress> generateRandomLocationAddresses() {

        List<LocationAddress> result = new LinkedList<>();

        for (int i = 0; i < 200; i++) {
            LocationAddress.Builder builder = LocationAddress.newBuilder()
                                                             .setStreet(randomString())
                                                             .setCity(randomString())
                                                             .setStateOrProvince(randomString())
                                                             .setZipOrPostalCode("" + ThreadLocalRandom.current().nextInt(10000, 99999))
                                                             .setCountryCode(randomString());

            result.add(builder.build());

        }

        return result;
    }

    private static String randomString() {
        return UUID
                .randomUUID().toString();
    }

}
