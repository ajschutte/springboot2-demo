package com.mercurygate.demo;

import java.util.function.Supplier;

import org.springframework.stereotype.Component;

@Component
public class DemoPrototypeComponentSupplier implements Supplier<DemoPrototypeComponent> {

    @Override
    public DemoPrototypeComponent get() {
        return new DemoPrototypeComponent();
    }

}
