package com.mercurygate.demo;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/demo-controller")
public class DemoRestController {

    private static final Logger logger = LoggerFactory.getLogger(DemoRestController.class);

    @Autowired
    private DemoSingletonService singleton;

    @Autowired
    private HttpServletRequest req;

    @GetMapping
    public DemoServiceResponses demo(@RequestParam Optional<Integer> serviceRequestCount) {

        logger.info("INVOKING doGet(..) on REST controller..");

        return singleton.demo(serviceRequestCount);

    }

}
