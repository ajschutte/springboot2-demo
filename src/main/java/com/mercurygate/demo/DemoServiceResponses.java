package com.mercurygate.demo;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DemoServiceResponses {

    private List<DemoServiceResponse> serviceResponses = new ArrayList<>();

    public List<DemoServiceResponse> getServiceResponses() {
        return serviceResponses;
    }

    public void addDemoServiceResponse(DemoServiceResponse response) {
        serviceResponses.add(response);
    }

}
