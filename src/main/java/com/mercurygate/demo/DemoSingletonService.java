package com.mercurygate.demo;

import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope(value = BeanDefinition.SCOPE_SINGLETON) //Default...
public class DemoSingletonService {

    private static final Logger logger = LoggerFactory.getLogger(DemoSingletonService.class);

    private static final int DEFAULT_COUNT = 3;

    //@Autowired
    //private DemoPrototypeComponent component;

    @Autowired
    private ObjectFactory<DemoPrototypeComponent> componentBuilder;

    @Autowired
    private Supplier<? extends DemoPrototypeComponent> componentSupplier;

    public DemoServiceResponses demo(Optional<Integer> serviceRequestCount) {

        logger.info("INVOKING demo() on singleton: {}", this);

        int requestCount = serviceRequestCount.orElse(DEFAULT_COUNT);

        DemoServiceResponses responses = new DemoServiceResponses();

        //DemoPrototypeComponent component = componentSupplier.get();
        DemoPrototypeComponent component = componentBuilder.getObject();

        IntStream.rangeClosed(1, requestCount).mapToObj(i -> component.demo()).forEach(responses::addDemoServiceResponse);

        return responses;

    }

}
