package com.mercurygate.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Andries on 2/8/18.
 */
@SpringBootApplication
@ComponentScan(basePackageClasses = {DemoRoot.class})
@ServletComponentScan(basePackageClasses = {DemoRoot.class})
public class DemoSpringAppConfig {

    private static final Logger logger = LoggerFactory.getLogger(DemoSpringAppConfig.class);

    public static void main(String[] args) {

        logger.debug("Starting up from main(..)..");

        new SpringApplicationBuilder(DemoSpringAppConfig.class)
                .bannerMode(Banner.Mode.CONSOLE)
                .logStartupInfo(true)
                .listeners((ApplicationListener<ApplicationEvent>) applicationEvent ->
                        logger.debug("Application Event: {}", applicationEvent.getClass().getCanonicalName()))
                .run(args);

    }

}
